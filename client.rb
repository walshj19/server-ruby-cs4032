require 'socket'
require 'thread'

class Client
	# declare the instance variables for the class
	def initialize
		@host = 'localhost'									# The sever address
		@port = 80													# Default HTTP port
		@path = "/"													# The file location
		@params = ""												# request parameters

		# http headers
		@send_headers = {
			"Host" => @host,
			"Connection" => "close",
			"Accept-Charset" => "ISO-8859-1,UTF-8;q=0.7,*;q=0.7",
			"Cache-Control" => "no-cache"
		}

		@del = "\r\n"
	end

	# setup the tcp connection and make the get request
	def start
		count = 0
		begin
			print count
			count += 1
			# Generate the request
			#request = gets
			request = "HELO"
			kill = request == "KILL_SERVICE\n"

			# start the connection and send the request
			tcp_socket = TCPSocket.open(@host,@port)						# Connect to server
			tcp_socket.puts request															# Send request
			response = tcp_socket.read													# Read complete response

			# display the response
			print response
		end until kill
	end
end

# start the program
if __FILE__ == $0
	Client.new.start
end
