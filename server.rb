require 'socket'

class Server
	def initialize
		@host = 'localhost'
		@port = 80
		@request_pattern = /^HELO.*/
	end

	def start
		print "Server started\n"

		# start server
		tcp_server = TCPServer.new @port

		begin
			# accept a client connection
			client = tcp_server.accept
			print "client connected\n"

			# get the client request
			request = client.gets
			print request+"\n"
			kill = request == "KILL_SERVICE\n"			#check if server should stop
			sleep 2

			#send response
			if (request =~ @request_pattern)
				response = request << "MORE"
			else
				response = "NONE"
			end
			client.puts response

			client.close
		end until kill
	end
end

if __FILE__ == $0
		Server.new.start
end
